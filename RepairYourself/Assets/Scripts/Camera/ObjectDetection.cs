﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetection : MonoBehaviour
{
    public const float MAX_RAY_DISTANCE = 3.5f;
    public MoveObject m_moveObject;
    public ItemPickup m_itemPickup;
    public OpenCloseObjects m_openCloseDoors;
    public JumpToObject m_jumpToObject;
    public InteractWithObjects m_interactWithObjects;
    public Inputs m_inputs;
    public bool m_characterIsUp = false;

    void Update()
    {
        rayCastDetection();
    }

    private void rayCastDetection()
    {
        Ray l_ray = new Ray(transform.position, transform.TransformDirection(Vector3.forward));
        RaycastHit l_hit;
        if (Physics.Raycast(l_ray, out l_hit, MAX_RAY_DISTANCE))
        {
            var l_object = l_hit.transform.gameObject;
            if (!m_characterIsUp)
            {
                m_moveObject.getObjectToHands(l_object);
            }
            m_itemPickup.usableItemDetection(l_object, m_interactWithObjects.m_iteractAction);
            handleOpeningClosingDoors(l_object);
            handleJumpToObject(l_object);
            m_interactWithObjects.interact(l_object);
        }
        else
        {
            m_itemPickup.resetMaterial();
        }
    }

    private void handleOpeningClosingDoors(GameObject p_object)
    {
        if (p_object.layer == LayerMask.NameToLayer("Doors"))
        {
            m_openCloseDoors = p_object.GetComponent<OpenCloseObjects>();
            if (m_inputs.mouseClick())
            {
                m_openCloseDoors.openClose();
            }
        }
    }
    
    private void handleJumpToObject(GameObject p_object)
    {
        if(p_object.layer == LayerMask.NameToLayer("MovableObjects"))
        {
            if(m_inputs.mouseSecondButtonClick() && !m_moveObject.m_isInHands)
            {
                m_jumpToObject.jumpToObject(p_object);
                m_characterIsUp = !m_characterIsUp;
            }
        }
    }
}
