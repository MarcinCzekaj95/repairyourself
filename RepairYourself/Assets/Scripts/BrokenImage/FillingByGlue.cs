﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillingByGlue : MonoBehaviour
{
    public GameLoopManager m_gameManager;
    public GameObject m_brokenImage;
    public GameObject m_fixedImage;
    public Mesh m_mesh;
    public Glue m_glue;
    private const int SECOND_SPRITE_INDEX = 1;

    // Update is called once per frame
    void Update()
    {
        fixBrokenImage();
    }

    private void fixBrokenImage()
    {
        if (FillPart.counter == 17)
        {
            m_brokenImage.layer = 8;
            m_fixedImage.GetComponent<MeshRenderer>().enabled = true;
            m_brokenImage.GetComponent<MeshFilter>().mesh = m_mesh;
            m_glue.m_glueIsUsed = true;
            m_gameManager.changeSprite(SECOND_SPRITE_INDEX);
            HighScore.s_score += ((int)m_gameManager.timer * 2);
            FillPart.counter = 0;
        }
    }
}
