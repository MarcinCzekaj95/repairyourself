﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillPart : MonoBehaviour
{
    public Glue m_glue;
    public static int counter = 0;

    void Start()
    {
        m_glue = GameObject.FindGameObjectWithTag("GlueFillPart").GetComponent<Glue>();
    }

    void OnMouseEnter()
    {
        if (m_glue.m_isActive)
        {
            this.GetComponent<MeshRenderer>().enabled = true;
            counter++;
        }
    }
}
