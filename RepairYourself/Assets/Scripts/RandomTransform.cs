﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTransform : MonoBehaviour
{
    public Transform m_first;
    public Transform m_second;
    public Transform m_third;

    void Start()
    {
        randomPosition();
    }

    private void randomPosition()
    {
        var i = Random.Range(0, 12);
        if(i <= 4)
        {
            Debug.Log("Random 1");
            transform.position = m_first.position;
        }
        else if(i > 4 && i <= 8)
        {
            Debug.Log("Random 2");
            transform.position = m_second.position;
        }
        else
        {
            Debug.Log("Random 3");
            transform.position = m_third.position;
        }
    }
}
