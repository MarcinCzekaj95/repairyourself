﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HangImageOnWall : MonoBehaviour
{
    public GameLoopManager m_gameManager;
    public bool m_playerStopRotatione = true;
    public Inputs m_inputs;
    public Canvas m_gameOverCanvas;
    public Image m_gameOverImage;
    public Sprite m_youDideSprite;
    public PlayerMove m_playerMove;
    public HighScore m_playerHighScore;
    public UseHammer m_hammer;

    void Update()
    {
        if (m_hammer.m_hammerIsUsed)
        {
            stopRotatingImage();
        }
        rotateObjectBetweenToAngles();
    }

    private void rotateObjectBetweenToAngles()
    {
        if (!m_playerStopRotatione)
        {
            float l_angle = Mathf.Sin(Time.time) * 20;
            this.transform.localRotation = Quaternion.AngleAxis(l_angle, Vector3.forward);
        }
    }

    private void stopRotatingImage()
    {
        if(m_inputs.getSpaceClick())
        {
            var valueToSubstract = this.transform.localRotation.z;
            int factor = 5;
            if (m_gameManager.timer <= 20.0f)
            {
                factor = 20;
            }
            Debug.Log("Image rotatione = " + Mathf.Abs(valueToSubstract) * 10000);
            var sub = (int)Mathf.Abs(valueToSubstract) * 10000;
            HighScore.s_score += (((int)m_gameManager.timer * factor));
            Debug.Log("Before" + HighScore.s_score);
            HighScore.s_score -= sub;
            Debug.Log("After" + HighScore.s_score);
            m_playerStopRotatione = true;
            m_gameOverImage.sprite = m_youDideSprite;
            m_gameOverCanvas.enabled = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
            Cursor.visible = true;
            Time.timeScale = 0;
            m_playerMove.enabled = false;
            m_playerHighScore.saveHighScore();
        }
    }
}
