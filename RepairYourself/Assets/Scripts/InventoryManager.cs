﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public List<GameObject> m_allGameObjects;
    public Inventory m_inventory;
    public List<Button> m_objectsImages;

    public void loadImages()
    {
        int index = 0;
        foreach (var obj in m_allGameObjects)
        {
            if (m_inventory.m_items.Contains(obj))
            {
                m_objectsImages[index].image.sprite = obj.GetComponent<ObjectImages>().m_visibleImage;
            }
            else
            {
                m_objectsImages[index].image.sprite = obj.GetComponent<ObjectImages>().m_hiddenImage;
            }
            index++;
        }
    }
}
