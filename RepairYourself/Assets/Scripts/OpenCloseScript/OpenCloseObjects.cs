﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectState
{
    Opening = 0,
    Closing = 1,
    Idle = 2
}

public class OpenCloseObjects : MonoBehaviour
{
    public Transform m_originPosition;
    public Transform m_targetPosition;
    private const float MAX_RAY_DISTANCE = 2.0f;
    private const float SPEED = 2.0f;
    public bool m_isOpened = false;
    public ObjectState m_doorState;

    void Update()
    {
        if (m_doorState == ObjectState.Opening)
        {
            open();
            if (checkIfDoorsHasOpened())
            {
                m_isOpened = true;
                m_doorState = ObjectState.Idle;
            }
        }
        if(m_doorState == ObjectState.Closing)
        {
            close();
            if (checkIfDoorsHasClosed())
            {
                m_isOpened = false;
                m_doorState = ObjectState.Idle;
            }
        }
    }

    private void open()
    {
        float l_step = SPEED * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, m_targetPosition.position, l_step);
    }

    private void close()
    {
        float l_step = SPEED * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, m_originPosition.position, l_step);
    }
    
    public void openClose()
    {
        if(m_isOpened)
        {
            m_doorState = ObjectState.Closing;
        }
        else
        {
            m_doorState = ObjectState.Opening;
        }
    }

    private bool checkIfDoorsHasOpened()
    {
        return Vector3.Distance(transform.position, m_targetPosition.position) < 0.1f;
    }

    private bool checkIfDoorsHasClosed()
    {
        return Vector3.Distance(transform.position, m_originPosition.position) < 0.1f;
    }
}
