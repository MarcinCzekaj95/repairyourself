﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseNail : MonoBehaviour
{
    public Texture2D m_cursorsTexture;
    public bool m_isActive = false;
    public bool m_haveNail = false;
    public bool m_nailIsUsed = false;

    public void activeNail()
    {
        if(!m_nailIsUsed && m_haveNail)
        {
            Cursor.SetCursor(m_cursorsTexture, Vector2.zero, CursorMode.ForceSoftware);
            m_isActive = !m_isActive;
        }
    }
}
