﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpToObject : MonoBehaviour
{
    public Inputs m_inputs;
    public bool m_stayOnObject = false;
    private Vector3 m_currentPosition;
    private Vector3 m_prevPosition;
    private const float PLAYER_HEIGHT = 1.5f;

    public void jumpToObject(GameObject p_object)
    {
        if (!m_stayOnObject)
        {
            m_prevPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            transform.position = p_object.transform.position + Vector3.up * PLAYER_HEIGHT;
            m_currentPosition = transform.position;
            m_stayOnObject = true;
        }
        else
        {
            m_stayOnObject = false;
            transform.position = m_prevPosition;
        }
    }
}
