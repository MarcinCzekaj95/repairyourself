﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour
{
    public float getVerticalAxis()
    {
        return Input.GetAxis("Vertical");
    }

    public bool escapeClick()
    {
        return Input.GetKeyDown(KeyCode.Escape);
    }

    public float getHorizontalAxis()
    {
        return Input.GetAxis("Horizontal");
    }

    public float getMouseXAxis()
    {
        return Input.GetAxis("Mouse X");
    }

    public float getMouseYAxis()
    {
        return Input.GetAxis("Mouse Y");
    }

    public bool mouseClick()
    {
        return Input.GetKeyDown(KeyCode.Mouse0);
    }

    public bool mouseSecondButtonClick()
    {
        return Input.GetKeyDown(KeyCode.Mouse1);
    }

    public bool getSpaceClick()
    {
        return Input.GetKeyDown(KeyCode.Space);
    }
}
