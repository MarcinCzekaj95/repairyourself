﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Rigidbody m_playerRigid;
    public JumpToObject m_jumpToObject;
    public InteractWithObjects m_interactWithObjects;
    public Inputs m_inputs;
    public Camera m_camera;
    public float m_playerSpeed = 0.5f;
    public float m_sensitivity = 90.0f;
    private float m_rotationeY;
    private const float MINIMAL_ROTATIONE = -90.0f;
    private const float MAXIMAL_ROTATIONE = 90.0f;

    void Awake()
    {
        freezeCursor();
    }

    // Update is called once per frame
    void Update()
    {
        float l_deltaTime = Time.deltaTime;
        if (!m_interactWithObjects.m_iteractAction)
        {
            freezeCursor();
            if (!m_jumpToObject.m_stayOnObject)
            {
                playerMovement(l_deltaTime);
            }
            playerRotatione(l_deltaTime);
        }
        else
        {
            unfreezeCursor();
        }
    }

    private void playerMovement(float p_deltaTime)
    {
        m_playerRigid.transform.Translate(Vector3.forward * m_playerSpeed * p_deltaTime * m_inputs.getVerticalAxis());
        m_playerRigid.transform.Translate(Vector3.right * m_playerSpeed * p_deltaTime * m_inputs.getHorizontalAxis());
    }

    private void playerRotatione(float p_deltaTime)
    {
        Vector3 rotationeX;
        rotationeX = Vector3.up * m_sensitivity * m_inputs.getMouseXAxis() * p_deltaTime;
        m_playerRigid.transform.rotation *= Quaternion.Euler(rotationeX);
        m_rotationeY -= m_inputs.getMouseYAxis() * m_sensitivity * p_deltaTime;
        m_rotationeY = Mathf.Clamp(m_rotationeY, MINIMAL_ROTATIONE, MAXIMAL_ROTATIONE);
        m_camera.transform.localRotation = Quaternion.Euler(m_rotationeY, 0, 0);
    }

    private void freezeCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void unfreezeCursor()
    {
        Cursor.lockState = CursorLockMode.None;
    }
}
