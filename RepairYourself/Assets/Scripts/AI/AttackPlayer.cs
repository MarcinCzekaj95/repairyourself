﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackPlayer : MonoBehaviour
{
    public Canvas m_gameOverCanvas;
    public Transform m_enemy;
    public Image m_gameOverImage;
    public Sprite m_youDideSprite;
    public PlayerMove m_playerMove;
    public Transform m_playerTransform;
    private float moveSpeed = 2.0f;
    public const float MIN_DISTANCE = 2.0f;
    public GameLoopManager m_gameLoop;
    public float timer = 4.0f;
    public Animator m_animator;

    void Update()
    {
        attackPlayerIfReady();
    }

    private void attackPlayerIfReady()
    {
        if (m_gameLoop.m_gameOver)
        {
            this.transform.LookAt(m_playerTransform);

            if (Vector3.Distance(transform.position, m_playerTransform.position) >= MIN_DISTANCE)
            {
                transform.position += transform.forward * moveSpeed * Time.deltaTime;
            }
            else
            {

                timer -= Time.deltaTime;
                m_playerMove.m_playerRigid.transform.LookAt(m_enemy);
                m_animator.SetBool("Attack", true);
                if (m_playerMove.enabled)
                {
                    m_playerMove.enabled = false;
                }
                if(timer <= 0.0f)
                {
                    m_gameOverImage.sprite = m_youDideSprite;
                    m_gameOverCanvas.enabled = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                }
            }
        }
    }
}
