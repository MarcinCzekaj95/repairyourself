﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseImage : MonoBehaviour
{
    public Texture2D m_cursorImage;
    public GameObject m_fixedImages;
    public UseHammer m_hammer;
    public Material m_material;

    public void activeImage()
    {
        if(m_hammer.m_hammerIsUsed)
        {
            Cursor.SetCursor(m_cursorImage, Vector2.zero, CursorMode.ForceSoftware);
            m_fixedImages.GetComponent<MeshRenderer>().material = m_material;
            m_fixedImages.GetComponent<HangImageOnWall>().m_playerStopRotatione = false;
        }
    }

}
