﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<GameObject> m_items;
    public Glue m_glue;
    public UseHammer m_hammer;
    public UseNail m_nail;

    public void pickUpItem(GameObject p_item)
    {
        if (p_item.tag.Equals("Glue"))
        {
            m_glue.m_haveGlue = true;
        }
        if (p_item.tag.Equals("Hammer"))
        {
            m_hammer.m_haveHammer = true;
        }
        if (p_item.tag.Equals("Nail"))
        {
            m_nail.m_haveNail = true;
        }
        m_items.Add(p_item);
    }
}
