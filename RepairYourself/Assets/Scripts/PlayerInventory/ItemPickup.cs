﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public Inventory m_inventory;
    public Inputs m_inputs;
    public Material m_highlightMaterial;
    private Material m_normalMaterial;
    public bool m_materialChanged = false;
    private GameObject m_currentHighlightedObject;
    public Shader m_highLightShader;
    public Shader m_normalShader;
    public Shader m_highLightShaderUsable;

    public void usableItemDetection(GameObject p_object, bool p_isIteract)
    {
        if (p_object.layer != LayerMask.NameToLayer("Default"))
        {
            if (!m_materialChanged && !p_isIteract)
            {
                m_currentHighlightedObject = p_object;
                m_highlightMaterial.shader = m_highLightShader;
                m_normalMaterial = p_object.GetComponent<Renderer>().material;
                p_object.GetComponent<Renderer>().material = m_highlightMaterial;
                m_materialChanged = true;
            }
        }
        else
        {
            resetMaterial();
        }
        if (p_object.layer == LayerMask.NameToLayer("Usable"))
        {
            m_highlightMaterial.shader = m_highLightShaderUsable;
            if (m_inputs.mouseClick())
            {
                m_inventory.pickUpItem(p_object);
                p_object.GetComponent<Renderer>().material = m_normalMaterial;
                p_object.SetActive(false);
            }
        }
    }

    public void resetMaterial()
    {
        if (m_currentHighlightedObject != null)
        {
            m_normalMaterial.shader = m_normalShader;
            m_currentHighlightedObject.GetComponent<Renderer>().material = m_normalMaterial;
            m_materialChanged = false;
        }
    }

}
