﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseHammer : MonoBehaviour
{
    public Texture2D m_cursorsTexture;
    public bool m_hammerIsActive = false;
    public bool m_haveHammer = false;
    public bool m_nailIsSet = false;
    public bool m_hammerIsUsed = false;

    public void activeHammer()
    {
        if(!m_hammerIsUsed && m_haveHammer && m_nailIsSet)
        {
            Cursor.SetCursor(m_cursorsTexture, Vector2.zero, CursorMode.ForceSoftware);
            m_hammerIsActive = !m_hammerIsActive;
        }
    }
}
