﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Canvas m_infoCanvas;
    public Canvas m_mainMenuCanvas;

    public void start()
    {
        SceneManager.LoadScene(1);
    }

    public void info()
    {
        m_mainMenuCanvas.enabled = false;
        m_infoCanvas.enabled = true;
    }

    public void back()
    {
        m_mainMenuCanvas.enabled = true;
        m_infoCanvas.enabled = false;
    }

    public void exit()
    {
        Application.Quit();
    }
}
