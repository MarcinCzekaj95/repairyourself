﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLoopManager : MonoBehaviour
{
    public float timer = 30.0f;
    public Text m_timerInfo;
    public bool m_gameOver = false;
    public GameObject m_fatherEnemy;
    public List<Sprite> m_sprites;
    public Image m_fatherFaceImage;

    // Update is called once per frame
    void Update()
    {
        updateTimer();
        updateRemaningTimeInfo();
    }

    private void updateTimer()
    {
        timer -= Time.deltaTime;
        if(timer <= 0.0f)
        {
            m_gameOver = true;
            m_fatherEnemy.SetActive(true);
        }
    }

    private void updateRemaningTimeInfo()
    {
        m_timerInfo.text = "Remainig time: " + timer.ToString("F2");
    }

    public void changeSprite(int i)
    {
        m_fatherFaceImage.sprite = m_sprites[i];
    }
}
