﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorsState
{
    Opening = 0,
    Closing = 1,
    Idle = 2
}

public class OpenCloseDoors : MonoBehaviour
{
    public Transform m_originPosition;
    public Transform m_targetPosition;
    private const float MAX_RAY_DISTANCE = 2.0f;
    private const float SPEED = 2.0f;
    public bool m_isOpened = false;
    public DoorsState m_doorState;

    void Update()
    {
        if (m_doorState == DoorsState.Opening)
        {
            openDoor();
            if (checkIfDoorsHasOpened())
            {
                m_isOpened = true;
                m_doorState = DoorsState.Idle;
            }
        }
        if(m_doorState == DoorsState.Closing)
        {
            closeDoor();
            if (checkIfDoorsHasClosed())
            {
                m_isOpened = false;
                m_doorState = DoorsState.Idle;
            }
        }
    }

    private void openDoor()
    {
        float l_step = SPEED * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, m_targetPosition.position, l_step);
    }

    private void closeDoor()
    {
        float l_step = SPEED * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, m_originPosition.position, l_step);
    }
    
    public void openCloseDoors()
    {
        if(m_isOpened)
        {
            m_doorState = DoorsState.Closing;
        }
        else
        {
            m_doorState = DoorsState.Opening;
        }
    }

    private bool checkIfDoorsHasOpened()
    {
        return Vector3.Distance(transform.position, m_targetPosition.position) < 0.1f;
    }

    private bool checkIfDoorsHasClosed()
    {
        return Vector3.Distance(transform.position, m_originPosition.position) < 0.1f;
    }
}
