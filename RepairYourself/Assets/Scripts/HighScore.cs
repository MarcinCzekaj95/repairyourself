﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{
    public Text m_playerHighScoreInfo;
    public static int s_score;

    void Start()
    {
        s_score = PlayerPrefs.GetInt("HighScore");
        Debug.Log("score on start: " + s_score.ToString());
        m_playerHighScoreInfo.text = " score: " + s_score.ToString();
        s_score = 0;
    }

    public void saveHighScore()
    {
        if (s_score >= PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", s_score);
        }
    }
}
