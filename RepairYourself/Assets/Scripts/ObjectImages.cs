﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectImages : MonoBehaviour
{
    public Sprite m_visibleImage;
    public Sprite m_hiddenImage;
}
