﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Canvas m_menuCanvas;
    public PlayerMove m_playerMove;
    public Inputs m_inputs;

    void Update()
    {
        if(m_inputs.escapeClick())
        {
            m_menuCanvas.enabled = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Time.timeScale = 0;
            m_playerMove.enabled = false;
        }
    }

    public void resumeGame()
    {
        m_menuCanvas.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        m_playerMove.enabled = true;
    }

    public void restartGame()
    {
        m_menuCanvas.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
        m_playerMove.enabled = true;
        SceneManager.LoadScene(1);
    }

    public void exit()
    {
        SceneManager.LoadScene(0);
    }

    public void exitGame()
    {
        Application.Quit();
    }

}
