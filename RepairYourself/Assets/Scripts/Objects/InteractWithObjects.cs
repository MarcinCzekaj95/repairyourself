﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    Idle = 0,
    Iteract = 1,
    FinishIteract = 2
}

public class InteractWithObjects : MonoBehaviour
{
    public InventoryManager m_inventoryManager;
    public Transform m_targetTransform;
    public Vector3 m_prevPostion;
    public Quaternion m_prevRotatione;
    public Inputs m_inputs;
    private const float SPEED = 2.0f;
    private const float SENSITITVITY = 90.0f;
    public PlayerState m_isInteract = PlayerState.Idle;
    public bool m_iteractAction = false;
    public Canvas m_inventoryCanvas;

    void Update()
    {
        float l_step = SPEED * Time.deltaTime;
        float l_sesitivity = SENSITITVITY * Time.deltaTime;
        if (m_isInteract == PlayerState.Iteract)
        {
            m_inventoryCanvas.enabled = true;
            Cursor.visible = true;
            m_inventoryManager.loadImages();
            transform.position = Vector3.MoveTowards(transform.position, m_targetTransform.position, l_step);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, m_targetTransform.rotation, l_sesitivity);
            m_iteractAction = true;
            if (Vector3.Distance(transform.position, m_targetTransform.position) < 0.1f)
            {
                m_isInteract = PlayerState.Idle;
            }
        }
        if(m_isInteract == PlayerState.FinishIteract)
        {
            m_inventoryCanvas.enabled = false;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
            Cursor.visible = false;
            transform.position = Vector3.MoveTowards(transform.position, m_prevPostion, l_step);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, m_prevRotatione, l_sesitivity);
            if (Vector3.Distance(transform.position, m_prevPostion) < 0.1f)
            {
                m_iteractAction = false;
                m_isInteract = PlayerState.Idle;
            }
        }
    }

    public void interact(GameObject p_object)
    {
        if(p_object.layer == LayerMask.NameToLayer("Iteractible"))
        {
            m_targetTransform = p_object.GetComponentInChildren<BoardPosition>().m_currentTransform;

            if(m_inputs.mouseClick() && !m_iteractAction)
            {
                m_prevPostion = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                m_prevRotatione = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, 1.0f);
                m_isInteract = PlayerState.Iteract;
            }
        }
        if (m_inputs.mouseSecondButtonClick() && m_iteractAction)
        {
            m_isInteract = PlayerState.FinishIteract;
        }
    }
}
