﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public const float MAX_RAY_DISTANCE = 2.0f;
    public const float OBJECT_HEIGHT = 0.5f;
    public Inputs m_inputs;
    public bool m_isInHands = false;
    private GameObject m_currentObject;

    void start()
    {
        m_isInHands = false;
    }

    void Update()
    {
        objectMove();
    }

    public void getObjectToHands(GameObject p_object)
    {
        if (p_object.layer == LayerMask.NameToLayer("MovableObjects"))
        {
            m_currentObject = p_object;
            if (m_inputs.mouseClick())
            {
                Physics.IgnoreCollision(p_object.GetComponent<Collider>(), GetComponent<Collider>());
                m_isInHands = !m_isInHands;
            }
        }
    }

    private void objectMove()
    {
        if (m_isInHands)
        {
            Vector3 l_playerPostion = new Vector3(transform.position.x, OBJECT_HEIGHT, transform.position.z);
            m_currentObject.transform.position = l_playerPostion + transform.forward * MAX_RAY_DISTANCE;
            return;
        }
        if (m_currentObject != null)
        {
            Physics.IgnoreCollision(m_currentObject.GetComponent<Collider>(), GetComponent<Collider>(), false);
        }
    }
}
