﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glue : MonoBehaviour
{
    public Texture2D m_glueCursor;
    public bool m_isActive = false;
    public bool m_haveGlue = false;
    public bool m_glueIsUsed = false;

    public void activeGlue()
    {
        if (!m_glueIsUsed && m_haveGlue)
        {
            Cursor.SetCursor(m_glueCursor, Vector2.zero, CursorMode.ForceSoftware);
            m_isActive = !m_isActive;
        }
    }
}
