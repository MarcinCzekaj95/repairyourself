﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixImagesOnWall : MonoBehaviour
{
    public GameLoopManager m_gameManager;
    public GameObject m_fixedImage;
    public UseNail m_nail;
    public UseHammer m_hammer;
    public GameObject m_nailObject;
    public Material m_material;
    private const int THIRD_SPRITE_INDEX = 2;
    private const int FOURTH_SPRITE_INDEX = 2;

    void OnMouseDown()
    {
        if(m_nail.m_isActive)
        {
            Debug.Log("umieszczenie gwozdzia");
            m_gameManager.changeSprite(THIRD_SPRITE_INDEX);
            m_hammer.m_nailIsSet = true;
            m_nailObject.GetComponent<MeshRenderer>().enabled = true;
            HighScore.s_score += ((int)m_gameManager.timer * 3);
            Debug.Log(HighScore.s_score);
        }

        if(m_hammer.m_hammerIsActive)
        {
            Debug.Log("uzycie mlotka");
            m_gameManager.changeSprite(FOURTH_SPRITE_INDEX);
            m_hammer.m_hammerIsUsed = true;
            m_nailObject.GetComponent<Transform>().Translate(Vector3.forward * 1.5f);
            HighScore.s_score += ((int)m_gameManager.timer * 4);
            Debug.Log(HighScore.s_score);
        }
    }
}
